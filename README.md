# 📢 Call for Proposals for WDH Monthly Meetups  
[Meetup](https://meetup.com/webdevhyd) | [Submit a Proposal](https://gitlab.com/webdevhyd/monthly-meetups/issues/new) | [Upvote a Proposal](https://gitlab.com/webdevhyd/monthly-meetups/issues/)

We are web developers and git is something that most of us use every day, so we decided to put CFP here.  
Web Developers Hyderabad Meetups are organized every month. It's always a Saturday afternoon or Sunday morning, and the exact dates are announced  as soon as possible.  

### How to Apply
Instead of sending an email or forking the repository, you can just create a [new issue](https://gitlab.com/webdevhyd/monthly-meetups/issues/new) in this repository. Use the "Meetup_CFP" issue template while creating the issue.  

Here is the information that you have to include - 

_A few things about you:_
* Your name
* Your email or any other way to contact you
* Tell us something about yourself
* [optional] Twitter and other handles

*And a few things about the topic:*
* Title or Topic
* Short description/abstract
* Preferred language
* Approximate duration of the talk

### How to Upvote
Open a proposal from the [issues list](https://gitlab.com/webdevhyd/monthly-meetups/issues) and upvote by clicking 👍 under the issue.

### Deadline
The CFP is open for all year long but one proposal is shortlisted every month.  

### Topics of Interest
We are open to any topic directly or indirectly related to Web.
Please try to avoid talks about your products unless your product is some  JS library (Browserify, Babel...), some Python framework (Django,  Bottle, Flask...) or something that would be really interesting for our audience.  

Don't worry if this is the *first time* you submit for CFPs. No one makes a perfect submission for the first time. Your submission will undergo several iterations and we will help you through the process.

### Participate in the community   
* RSVP in [Meetup](https://meetup.com/webdevhyd)  
* Join in [Telegram](https://t.me/webdevhyd)  
* Follow in [Twitter](https://twitter.com/webdevhyd)
* Mail to webdevhyd@disroot.org
