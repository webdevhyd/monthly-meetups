Fill the Issue Title as follows: [Talk/Workshop] - [Title/Topic]  
**Remove this section after filling the issue title.**  

#### Expertise
[Beginner/Intermediate/Advanced]

#### [Title/Topic]  
[A Short description/abstract of the proposal structured in brief.] 

#### Duration  
[The duration of the proposal including breaks/hands-on if any.]  

#### Pre-requisites and Material
[Feel free to include slides, links to slides, reference materials or anything that can help the people to know what to expect. Also specify pre-requisites for the people attending if there are any.]  

#### [Your Name]  
[Tell us something about yourself. Below the description, attach your picture of *500 X 500 px* dimensions.]   

**Contact:** Your email or any other way to contact you  
**Follow:** Links for Twitter and other handles  
